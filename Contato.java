/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.io.Serializable;

/**
 *
 * @author sergi
 */
public class Contato implements Serializable{
    private String nome;
    private String ddd1;
    private String ddd2;
    private String ddd3;
    private String operadora1;
    private String operadora2;
    private String operadora3;
    private String telefone1;
    private String telefone2;
    private String telefone3;
    private String email1;
    private String email2;
    private String email3;
    private String rua;
    private String complemento;
    private String ruaN;
    private String pais;
    private String cidade;
    private String bairro;
    private String cep; 
    private String estado;
    private String aniversario;
    private String grupo;

    public Contato(String nome,String ddd1, String telefone1,String operadora1,String ddd2, String telefone2,String operadora2,String ddd3, String telefone3,String operadora3, String email1, String email2, String email3, String rua, String complemento, String ruaN, String pais, String cidade, String bairro, String cep, String estado, String aniversario, String grupo) {
        this.nome = nome;
        this.ddd1 = ddd1;
        this.ddd2 = ddd2;
        this.ddd3 = ddd3;
        this.operadora1 = operadora1;
        this.operadora2 = operadora2;
        this.operadora3 = operadora3;
        this.telefone1 = telefone1;
        this.telefone2 = telefone2;
        this.telefone3 = telefone3;
        this.email1 = email1;
        this.email2 = email2;
        this.email3 = email3;
        this.rua = rua;
        this.complemento = complemento;
        this.ruaN = ruaN;
        this.pais = pais;
        this.cidade = cidade;
        this.bairro = bairro;
        this.cep = cep;
        this.estado = estado;
        this.aniversario = aniversario;
        this.grupo = grupo;
    }
    public Contato(){
        
    }

    public String getNome() {
        return nome;
    }

    public String getDdd1() {
        return ddd1;
    }

    public String getDdd2() {
        return ddd2;
    }

    public String getDdd3() {
        return ddd3;
    }

    public String getOperadora1() {
        return operadora1;
    }

    public String getOperadora2() {
        return operadora2;
    }

    public String getOperadora3() {
        return operadora3;
    }

    public String getTelefone1() {
        return telefone1;
    }

    public String getTelefone2() {
        return telefone2;
    }

    public String getTelefone3() {
        return telefone3;
    }

    public String getEmail1() {
        return email1;
    }

    public String getEmail2() {
        return email2;
    }

    public String getEmail3() {
        return email3;
    }

    public String getRua() {
        return rua;
    }

    public String getComplemento() {
        return complemento;
    }

    public String getRuaN() {
        return ruaN;
    }

    public String getPais() {
        return pais;
    }

    public String getCidade() {
        return cidade;
    }

    public String getBairro() {
        return bairro;
    }

    public String getCep() {
        return cep;
    }

    public String getEstado() {
        return estado;
    }

    public String getAniversario() {
        return aniversario;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setDdd1(String ddd1) {
        this.ddd1 = ddd1;
    }

    public void setDdd2(String ddd2) {
        this.ddd2 = ddd2;
    }

    public void setDdd3(String ddd3) {
        this.ddd3 = ddd3;
    }

    public void setOperadora1(String operadora1) {
        this.operadora1 = operadora1;
    }

    public void setOperadora2(String operadora2) {
        this.operadora2 = operadora2;
    }

    public void setOperadora3(String operadora3) {
        this.operadora3 = operadora3;
    }

    public void setTelefone1(String telefone1) {
        this.telefone1 = telefone1;
    }

    public void setTelefone2(String telefone2) {
        this.telefone2 = telefone2;
    }

    public void setTelefone3(String telefone3) {
        this.telefone3 = telefone3;
    }

    public void setEmail1(String email1) {
        this.email1 = email1;
    }

    public void setEmail2(String email2) {
        this.email2 = email2;
    }

    public void setEmail3(String email3) {
        this.email3 = email3;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public void setRuaN(String ruaN) {
        this.ruaN = ruaN;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public void setAniversario(String aniversario) {
        this.aniversario = aniversario;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }


    
    
}
