
package Classes;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;


public class TelaTrabalho extends javax.swing.JInternalFrame {
   private ArrayList<Trabalho> arrayTrabalho;
    
    public TelaTrabalho() {

        initComponents();
        arrayTrabalho = new ArrayList();
        carregarEmArquivo();
        limparCampos();
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel9 = new javax.swing.JPanel();
        jLabel217 = new javax.swing.JLabel();
        jLabel218 = new javax.swing.JLabel();
        nome = new javax.swing.JTextField();
        jLabel219 = new javax.swing.JLabel();
        jLabel220 = new javax.swing.JLabel();
        ddd1 = new javax.swing.JTextField();
        telefone1 = new javax.swing.JTextField();
        jLabel221 = new javax.swing.JLabel();
        operadora1 = new javax.swing.JTextField();
        jLabel222 = new javax.swing.JLabel();
        jLabel223 = new javax.swing.JLabel();
        rua = new javax.swing.JTextField();
        jLabel224 = new javax.swing.JLabel();
        complemento = new javax.swing.JTextField();
        jLabel225 = new javax.swing.JLabel();
        n = new javax.swing.JTextField();
        jLabel226 = new javax.swing.JLabel();
        pais = new javax.swing.JTextField();
        jLabel227 = new javax.swing.JLabel();
        cidade = new javax.swing.JTextField();
        jLabel228 = new javax.swing.JLabel();
        bairro = new javax.swing.JTextField();
        jLabel229 = new javax.swing.JLabel();
        jLabel230 = new javax.swing.JLabel();
        aniver = new javax.swing.JFormattedTextField();
        jLabel231 = new javax.swing.JLabel();
        reu1 = new javax.swing.JFormattedTextField();
        cep = new javax.swing.JFormattedTextField();
        jButton9 = new javax.swing.JButton();
        jLabel232 = new javax.swing.JLabel();
        email1 = new javax.swing.JTextField();
        jLabel233 = new javax.swing.JLabel();
        estado = new javax.swing.JTextField();
        jLabel234 = new javax.swing.JLabel();
        ddd2 = new javax.swing.JTextField();
        telefone2 = new javax.swing.JTextField();
        jLabel235 = new javax.swing.JLabel();
        operadora2 = new javax.swing.JTextField();
        jLabel236 = new javax.swing.JLabel();
        telefone3 = new javax.swing.JTextField();
        jLabel237 = new javax.swing.JLabel();
        operadora3 = new javax.swing.JTextField();
        jLabel238 = new javax.swing.JLabel();
        jLabel239 = new javax.swing.JLabel();
        ddd3 = new javax.swing.JTextField();
        jLabel240 = new javax.swing.JLabel();
        email2 = new javax.swing.JTextField();
        jLabel241 = new javax.swing.JLabel();
        email3 = new javax.swing.JTextField();
        jLabel242 = new javax.swing.JLabel();
        reu2 = new javax.swing.JFormattedTextField();
        reu3 = new javax.swing.JFormattedTextField();
        jLabel243 = new javax.swing.JLabel();
        jScrollPane10 = new javax.swing.JScrollPane();
        tabelaTrab = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jLabel244 = new javax.swing.JLabel();
        empresa = new javax.swing.JTextField();
        jLabel245 = new javax.swing.JLabel();
        setor = new javax.swing.JTextField();
        jButton5 = new javax.swing.JButton();
        dias = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);

        jPanel9.setPreferredSize(new java.awt.Dimension(1280, 720));

        jLabel217.setFont(new java.awt.Font("Arial", 0, 20)); // NOI18N
        jLabel217.setText("Trabalho");

        jLabel218.setText("Nome:");

        jLabel219.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel219.setText("Telefone 1");

        jLabel220.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel220.setText("DDD:");

        jLabel221.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel221.setText("Operadora:");

        jLabel222.setText("Endereço");

        jLabel223.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel223.setText("Rua:");

        jLabel224.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel224.setText("Complemento:");

        jLabel225.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel225.setText("N°:");

        jLabel226.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel226.setText("País:");

        pais.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                paispaisActionPerformed(evt);
            }
        });

        jLabel227.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel227.setText("Cidade:");

        jLabel228.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel228.setText("Bairro:");

        jLabel229.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel229.setText("Cep:");

        jLabel230.setText("Anivesário:");

        try {
            aniver.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        aniver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aniveraniverActionPerformed(evt);
            }
        });

        jLabel231.setText("Reunião 1:");

        try {
            reu1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        try {
            cep.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("#####-###")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jButton9.setText("Adicionar");
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9jButton1ActionPerformed(evt);
            }
        });

        jLabel232.setText("Email 1:");

        jLabel233.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel233.setText("Estado:");

        jLabel234.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel234.setText("DDD:");

        ddd2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ddd2ddd2ActionPerformed(evt);
            }
        });

        jLabel235.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel235.setText("Operadora:");

        jLabel236.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel236.setText("Telefone 2");

        telefone3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                telefone3ActionPerformed(evt);
            }
        });

        jLabel237.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel237.setText("Operadora:");

        jLabel238.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel238.setText("Telefone 2");

        jLabel239.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel239.setText("DDD:");

        jLabel240.setText("Email 2:");

        jLabel241.setText("Email 3:");

        jLabel242.setText("Reunião 2:");

        try {
            reu2.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        try {
            reu3.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLabel243.setText("Reunião 3:");

        tabelaTrab.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nome", "DDD 1", "Telefone 1", "Operadora 1", "DDD 2", "Telefone 2", "Operadora 2", "DDD 3", "Telefone 3", "Operadora 3", "Email 1", "Email 2", "Email 3", "Rua", "Complemento", "Número", "País", "Cidade", "Bairro", "Cep", "Estado", "Aniversário", "Empresa", "Setor", "Reunião 1", "Reunião 2", "Reunião 3"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, true, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, true, true, false, false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tabelaTrab.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tabelaTrab.setPreferredSize(new java.awt.Dimension(1280, 720));
        tabelaTrab.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabelaTrabtabelaamgMouseClicked(evt);
            }
        });
        tabelaTrab.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                tabelaTrabPropertyChange(evt);
            }
        });
        tabelaTrab.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tabelaTrabKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tabelaTrabtabelaamgKeyReleased(evt);
            }
        });
        jScrollPane10.setViewportView(tabelaTrab);

        jButton1.setText("Remover");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Salvar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton4.setText("Editar");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jLabel244.setText("Empresa:");

        empresa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                empresapaisActionPerformed(evt);
            }
        });

        jLabel245.setText("Setor:");

        setor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                setorpaisActionPerformed(evt);
            }
        });

        jButton5.setText("Pesquisar");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        dias.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        dias.setText("0");

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 10)); // NOI18N
        jLabel1.setText("dias para seu aniversário");

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel9Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel9Layout.createSequentialGroup()
                                        .addComponent(jLabel218)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(nome))
                                    .addGroup(jPanel9Layout.createSequentialGroup()
                                        .addComponent(jLabel239)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(ddd3, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(telefone3, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(6, 6, 6)
                                        .addComponent(jLabel237)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(operadora3))
                                    .addGroup(jPanel9Layout.createSequentialGroup()
                                        .addComponent(jLabel232)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(email1))
                                    .addGroup(jPanel9Layout.createSequentialGroup()
                                        .addComponent(jLabel240)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(email2))
                                    .addGroup(jPanel9Layout.createSequentialGroup()
                                        .addComponent(jLabel241)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(email3))
                                    .addGroup(jPanel9Layout.createSequentialGroup()
                                        .addComponent(jLabel234)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(ddd2, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(147, 147, 147)
                                        .addComponent(jLabel235)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(operadora2))
                                    .addGroup(jPanel9Layout.createSequentialGroup()
                                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addGroup(jPanel9Layout.createSequentialGroup()
                                                .addComponent(jLabel220)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(ddd1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(telefone1, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addComponent(telefone2, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(6, 6, 6)
                                        .addComponent(jLabel221)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(operadora1, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addGroup(jPanel9Layout.createSequentialGroup()
                                                    .addComponent(jLabel223)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                    .addComponent(rua))
                                                .addComponent(jLabel222)
                                                .addGroup(jPanel9Layout.createSequentialGroup()
                                                    .addComponent(jLabel226)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                    .addComponent(pais, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                    .addComponent(jLabel227)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                    .addComponent(cidade, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                    .addComponent(jLabel225)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                    .addComponent(n))
                                                .addGroup(jPanel9Layout.createSequentialGroup()
                                                    .addComponent(jLabel224)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                    .addComponent(complemento, javax.swing.GroupLayout.PREFERRED_SIZE, 321, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGroup(jPanel9Layout.createSequentialGroup()
                                                    .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jLabel229)
                                                        .addComponent(jLabel228))
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                    .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(bairro)
                                                        .addGroup(jPanel9Layout.createSequentialGroup()
                                                            .addComponent(cep, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                            .addComponent(jLabel233)
                                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                            .addComponent(estado)))))
                                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                                                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                                                                .addComponent(jLabel243)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                                .addComponent(reu3, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                            .addGroup(jPanel9Layout.createSequentialGroup()
                                                                .addComponent(jLabel242)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                                .addComponent(reu2, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel9Layout.createSequentialGroup()
                                                            .addComponent(jLabel231)
                                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                            .addComponent(reu1, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel9Layout.createSequentialGroup()
                                                            .addComponent(jLabel244)
                                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                            .addComponent(empresa, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(jPanel9Layout.createSequentialGroup()
                                                            .addComponent(jLabel245)
                                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                            .addComponent(setor, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                    .addGroup(jPanel9Layout.createSequentialGroup()
                                                        .addComponent(jLabel230)
                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                        .addComponent(aniver, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                .addGap(22, 22, 22)
                                                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                    .addComponent(jButton4)
                                                    .addComponent(jButton9, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(23, 23, 23))))))
                            .addGroup(jPanel9Layout.createSequentialGroup()
                                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel9Layout.createSequentialGroup()
                                        .addGap(138, 138, 138)
                                        .addComponent(jLabel236))
                                    .addGroup(jPanel9Layout.createSequentialGroup()
                                        .addGap(135, 135, 135)
                                        .addComponent(jLabel219)))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addGap(137, 137, 137)
                        .addComponent(jLabel238)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane10, javax.swing.GroupLayout.PREFERRED_SIZE, 786, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton2)
                        .addGap(91, 91, 91)
                        .addComponent(jButton5)
                        .addGap(149, 149, 149)
                        .addComponent(dias)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(816, 816, 816))
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGap(216, 216, 216)
                .addComponent(jLabel217)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel9Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jButton1, jButton2});

        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addComponent(jLabel217)
                .addGap(12, 12, 12)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel218))
                .addGap(10, 10, 10)
                .addComponent(jLabel219)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel220)
                    .addComponent(ddd1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(telefone1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel221)
                    .addComponent(operadora1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel236)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel234)
                    .addComponent(ddd2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(telefone2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel235)
                    .addComponent(operadora2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel238)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel239)
                    .addComponent(ddd3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(telefone3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel237)
                    .addComponent(operadora3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(24, 24, 24)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel232)
                    .addComponent(email1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel240)
                    .addComponent(email2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel241)
                    .addComponent(email3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addComponent(jLabel222)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel223)
                            .addComponent(rua, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel224)
                            .addComponent(complemento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel226)
                                .addComponent(pais, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel227)
                                .addComponent(cidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(n, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel225)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel228)
                            .addComponent(bairro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel229)
                            .addComponent(cep, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel233)
                            .addComponent(estado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(12, 12, 12)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel9Layout.createSequentialGroup()
                                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel230)
                                    .addComponent(aniver, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel231)
                                    .addComponent(reu1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel242)
                                    .addComponent(reu2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel243)
                                    .addComponent(reu3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                                .addComponent(jButton9, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButton4)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel244)
                            .addComponent(empresa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(setor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel245)))))
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton5, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(dias, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jButton2)
                        .addComponent(jButton1)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane10, javax.swing.GroupLayout.PREFERRED_SIZE, 562, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel9Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jButton1, jButton2});

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, 1227, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(14, Short.MAX_VALUE)
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, 636, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(14, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void paispaisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_paispaisActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_paispaisActionPerformed

    private void aniveraniverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aniveraniverActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_aniveraniverActionPerformed

    private void jButton9jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9jButton1ActionPerformed
        //define o modelo default
        DefaultTableModel mdltable = (DefaultTableModel) tabelaTrab.getModel();
        //adiciona uma nova linha com todas as informações inseridas
        mdltable.addRow(new Object[]{nome.getText(),ddd1.getText(),telefone1.getText(),operadora1.getText(),ddd2.getText(),telefone2.getText(),operadora2.getText(),ddd3.getText(),telefone3.getText(),operadora3.getText(),email1.getText(),email2.getText(),email3.getText(),rua.getText(),complemento.getText(),n.getText(),pais.getText(),cidade.getText(),bairro.getText(),cep.getText(),estado.getText(),aniver.getText(),empresa.getText(),setor.getText(),reu1.getText(),reu2.getText(),reu3.getText()});
        //criar novo objeto trabalho
        Trabalho t1 = new Trabalho(nome.getText(),ddd1.getText(),telefone1.getText(),operadora1.getText(),ddd2.getText(),telefone2.getText(),operadora2.getText(),ddd3.getText(),telefone3.getText(),operadora3.getText(),email1.getText(),email2.getText(),email3.getText(),rua.getText(),complemento.getText(),n.getText(),pais.getText(),cidade.getText(),bairro.getText(),cep.getText(),estado.getText(),aniver.getText(),empresa.getText(),setor.getText(),reu1.getText(),reu2.getText(),reu3.getText());
        //armazenar no arraylist
        arrayTrabalho.add(t1);
        //limpar campos
        limparCampos();
    }//GEN-LAST:event_jButton9jButton1ActionPerformed

    private void ddd2ddd2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ddd2ddd2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ddd2ddd2ActionPerformed

    private void telefone3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_telefone3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_telefone3ActionPerformed

    private void tabelaTrabtabelaamgMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabelaTrabtabelaamgMouseClicked
        setarCampos();
       try {
           diasParaAniver();
       } catch (ParseException ex) {
           Logger.getLogger(TelaTrabalho.class.getName()).log(Level.SEVERE, null, ex);
       }
    }//GEN-LAST:event_tabelaTrabtabelaamgMouseClicked

    private void tabelaTrabtabelaamgKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tabelaTrabtabelaamgKeyReleased
        setarCampos();
        try {
           diasParaAniver();
       } catch (ParseException ex) {
           Logger.getLogger(TelaTrabalho.class.getName()).log(Level.SEVERE, null, ex);
       }
    }//GEN-LAST:event_tabelaTrabtabelaamgKeyReleased

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
      int linhaSelecionada = tabelaTrab.getSelectedRow();
    if(tabelaTrab.getSelectedRow() !=-1);
    
    DefaultTableModel mdltable = (DefaultTableModel) tabelaTrab.getModel();
    mdltable.removeRow(tabelaTrab.getSelectedRow());
    arrayTrabalho.remove(linhaSelecionada);
    int quantosRegistros = tabelaTrab.getRowCount();
    if(quantosRegistros > linhaSelecionada){
        tabelaTrab.setRowSelectionInterval(quantosRegistros, linhaSelecionada);
        setarCampos();
    }
        else if(tabelaTrab.getRowCount() > 0){
            tabelaTrab.setRowSelectionInterval(linhaSelecionada-1, linhaSelecionada-1);
            setarCampos();
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        gravarEmArquivo();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void tabelaTrabPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_tabelaTrabPropertyChange
        //System.out.println("Mudou");
    }//GEN-LAST:event_tabelaTrabPropertyChange

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
    int selecionado = tabelaTrab.getSelectedRow();
   //  atualizarValoresTabela(selecionado); //atualiza na tabela
    arrayTrabalho.get(selecionado).setTrabalho(nome.getText(),ddd1.getText(),telefone1.getText(),operadora1.getText(),ddd2.getText(),telefone2.getText(),operadora2.getText(),ddd3.getText(),telefone3.getText(),operadora3.getText(),email1.getText(),email2.getText(),email3.getText(),rua.getText(),complemento.getText(),n.getText(),pais.getText(),cidade.getText(),bairro.getText(),cep.getText(),estado.getText(),aniver.getText(),empresa.getText(),setor.getText(),reu1.getText(),reu2.getText(),reu3.getText());
    //gravarEmArquivo();
    }//GEN-LAST:event_jButton4ActionPerformed

    private void tabelaTrabKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tabelaTrabKeyPressed
        //setarCampos(tabela.getSelectedRow());
    }//GEN-LAST:event_tabelaTrabKeyPressed

    private void setorpaisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_setorpaisActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_setorpaisActionPerformed

    private void empresapaisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_empresapaisActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_empresapaisActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        boolean encontrou = false;
        String pesquisarNome = JOptionPane.showInputDialog("Digite um nome para pesquisar");
        if (pesquisarNome.length() > 0) {       //verifica se digitou algo
            for(int i=0; i<tabelaTrab.getRowCount();i++){        //laço para percorrer os nomes na tabela
                if (pesquisarNome.equals(tabelaTrab.getValueAt(i, 0))){           //se o i for igual ao que foi pesquisado, seta os campos do contato
                    nome.setText(tabelaTrab.getValueAt(i,0).toString());
                    ddd1.setText(tabelaTrab.getValueAt(i,1).toString());
                    telefone1.setText(tabelaTrab.getValueAt(i,2).toString());
                    operadora1.setText(tabelaTrab.getValueAt(i,3).toString());
                    ddd2.setText(tabelaTrab.getValueAt(i,4).toString());
                    telefone2.setText(tabelaTrab.getValueAt(i,5).toString());     
                    operadora2.setText(tabelaTrab.getValueAt(i,6).toString());
                    ddd3.setText(tabelaTrab.getValueAt(i,7).toString());
                    telefone3.setText(tabelaTrab.getValueAt(i,8).toString());
                    operadora3.setText(tabelaTrab.getValueAt(i,9).toString());
                    email1.setText(tabelaTrab.getValueAt(i,10).toString());
                    email2.setText(tabelaTrab.getValueAt(i,11).toString());
                    email3.setText(tabelaTrab.getValueAt(i,12).toString());
                    rua.setText(tabelaTrab.getValueAt(i,13).toString());
                    complemento.setText(tabelaTrab.getValueAt(i,14).toString());
                    n.setText(tabelaTrab.getValueAt(i,15).toString());
                    pais.setText(tabelaTrab.getValueAt(i,16).toString());
                    cidade.setText(tabelaTrab.getValueAt(i,17).toString());
                    bairro.setText(tabelaTrab.getValueAt(i,18).toString());
                    cep.setText(tabelaTrab.getValueAt(i,19).toString());
                    estado.setText(tabelaTrab.getValueAt(i,20).toString());
                    aniver.setText(tabelaTrab.getValueAt(i,21).toString());
                    empresa.setText(tabelaTrab.getValueAt(i,22).toString());
                    setor.setText(tabelaTrab.getValueAt(i,23).toString());
                    reu1.setText(tabelaTrab.getValueAt(i,24).toString());
                    reu2.setText(tabelaTrab.getValueAt(i,25).toString());
                    reu3.setText(tabelaTrab.getValueAt(i,26).toString());
                    encontrou = true;
                    break;
                }
            }
        }
        if (encontrou == false) {
            JOptionPane.showMessageDialog(null, "Você digitou errado ou o contato " + pesquisarNome + " não existe");
        }
    }//GEN-LAST:event_jButton5ActionPerformed

    public void limparCampos(){
        nome.setText("");
        ddd1.setText("");
        telefone1.setText("");
        operadora1.setText("");
        ddd2.setText("");
        telefone2.setText("");     
        operadora2.setText("");
        ddd3.setText("");
        telefone3.setText("");
        operadora3.setText("");
        email1.setText("");
        email2.setText("");
        email3.setText("");
        rua.setText("");
        complemento.setText("");
        n.setText("");
        pais.setText("");
        cidade.setText("");
        bairro.setText("");
        cep.setText("");
        estado.setText("");
        aniver.setText("");
        //modificado pelo grupo
        empresa.setText("");
        setor.setText("");
        reu1.setText("");
        reu2.setText("");
        reu3.setText("");
    }
    
    public void setarCampos(){
        int indice = tabelaTrab.getSelectedRow();
        nome.setText(tabelaTrab.getValueAt(indice,0).toString());
        ddd1.setText(tabelaTrab.getValueAt(indice,1).toString());
        telefone1.setText(tabelaTrab.getValueAt(indice,2).toString());
        operadora1.setText(tabelaTrab.getValueAt(indice,3).toString());
        ddd2.setText(tabelaTrab.getValueAt(indice,4).toString());
        telefone2.setText(tabelaTrab.getValueAt(indice,5).toString());     
        operadora2.setText(tabelaTrab.getValueAt(indice,6).toString());
        ddd3.setText(tabelaTrab.getValueAt(indice,7).toString());
        telefone3.setText(tabelaTrab.getValueAt(indice,8).toString());
        operadora3.setText(tabelaTrab.getValueAt(indice,9).toString());
        email1.setText(tabelaTrab.getValueAt(indice,10).toString());
        email2.setText(tabelaTrab.getValueAt(indice,11).toString());
        email3.setText(tabelaTrab.getValueAt(indice,12).toString());
        rua.setText(tabelaTrab.getValueAt(indice,13).toString());
        complemento.setText(tabelaTrab.getValueAt(indice,14).toString());
        n.setText(tabelaTrab.getValueAt(indice,15).toString());
        pais.setText(tabelaTrab.getValueAt(indice,16).toString());
        cidade.setText(tabelaTrab.getValueAt(indice,17).toString());
        bairro.setText(tabelaTrab.getValueAt(indice,18).toString());
        cep.setText(tabelaTrab.getValueAt(indice,19).toString());
        estado.setText(tabelaTrab.getValueAt(indice,20).toString());
        aniver.setText(tabelaTrab.getValueAt(indice,21).toString());
        //modificado pelo grupo
        empresa.setText(tabelaTrab.getValueAt(indice,22).toString());
        setor.setText(tabelaTrab.getValueAt(indice,23).toString());
        reu1.setText(tabelaTrab.getValueAt(indice,24).toString());
        reu2.setText(tabelaTrab.getValueAt(indice,25).toString());
        reu3.setText(tabelaTrab.getValueAt(indice,26).toString());
    }
    
    public void atualizarValoresTabela(int selecionado){
        tabelaTrab.setValueAt(nome.getText(), selecionado, 0);
        tabelaTrab.setValueAt(ddd1.getText(), selecionado, 1);
        tabelaTrab.setValueAt(telefone1.getText(), selecionado, 2);
        tabelaTrab.setValueAt(operadora1.getText(), selecionado, 3);
        tabelaTrab.setValueAt(ddd2.getText(), selecionado, 4);
        tabelaTrab.setValueAt(telefone2.getText(), selecionado, 5);
        tabelaTrab.setValueAt(operadora2.getText(), selecionado, 6);
        tabelaTrab.setValueAt(ddd3.getText(), selecionado, 7);
        tabelaTrab.setValueAt(telefone3.getText(), selecionado, 8);
        tabelaTrab.setValueAt(operadora3.getText(), selecionado, 9);
        tabelaTrab.setValueAt(email1.getText(), selecionado, 10);
        tabelaTrab.setValueAt(email2.getText(), selecionado, 11);
        tabelaTrab.setValueAt(email3.getText(), selecionado, 12);
        tabelaTrab.setValueAt(rua.getText(), selecionado, 13);
        tabelaTrab.setValueAt(complemento.getText(), selecionado, 14);
        tabelaTrab.setValueAt(n.getText(), selecionado, 15);
        tabelaTrab.setValueAt(pais.getText(), selecionado, 16);
        tabelaTrab.setValueAt(cidade.getText(), selecionado, 17);
        tabelaTrab.setValueAt(bairro.getText(), selecionado, 18);
        tabelaTrab.setValueAt(cep.getText(), selecionado, 19);
        tabelaTrab.setValueAt(estado.getText(), selecionado, 20);
        tabelaTrab.setValueAt(aniver.getText(), selecionado, 21);
        //modificado pelo grupo
        tabelaTrab.setValueAt(empresa.getText(), selecionado, 22);
        tabelaTrab.setValueAt(setor.getText(), selecionado, 23);
        tabelaTrab.setValueAt(reu1.getText(), selecionado, 24);
        tabelaTrab.setValueAt(reu2.getText(), selecionado, 25);
        tabelaTrab.setValueAt(reu3.getText(), selecionado, 26);      
    }
    
    public void gravarEmArquivo(){
    int quantos = tabelaTrab.getRowCount();
    arrayTrabalho.clear();
    for (int i=0;i<quantos;i++)
    arrayTrabalho.add(new Trabalho(tabelaTrab.getValueAt(i, 0).toString(),tabelaTrab.getValueAt(i, 1).toString(),tabelaTrab.getValueAt(i, 2).toString(),tabelaTrab.getValueAt(i, 3).toString(),tabelaTrab.getValueAt(i, 4).toString(),tabelaTrab.getValueAt(i, 5).toString(),tabelaTrab.getValueAt(i, 6).toString(),tabelaTrab.getValueAt(i, 7).toString(),tabelaTrab.getValueAt(i, 8).toString(),tabelaTrab.getValueAt(i, 9).toString(),tabelaTrab.getValueAt(i, 10).toString(),tabelaTrab.getValueAt(i, 11).toString(),tabelaTrab.getValueAt(i, 12).toString(),tabelaTrab.getValueAt(i, 13).toString(),tabelaTrab.getValueAt(i, 14).toString(),tabelaTrab.getValueAt(i, 15).toString(),tabelaTrab.getValueAt(i, 16).toString(),tabelaTrab.getValueAt(i, 17).toString(),tabelaTrab.getValueAt(i, 18).toString(),tabelaTrab.getValueAt(i, 19).toString(),tabelaTrab.getValueAt(i, 20).toString(),tabelaTrab.getValueAt(i, 21).toString(),tabelaTrab.getValueAt(i, 22).toString(),tabelaTrab.getValueAt(i, 23).toString(),tabelaTrab.getValueAt(i, 24).toString(),tabelaTrab.getValueAt(i, 25).toString(),tabelaTrab.getValueAt(i, 26).toString()));
    try{ 
    ObjectOutputStream arquivoGravar= new ObjectOutputStream(new FileOutputStream(new File("trabalho.txt")));
    arquivoGravar.writeObject(arrayTrabalho);
    JOptionPane.showMessageDialog(null,"Arquivo Gravado com sucesso");     
    } 
catch(IOException erro){
    JOptionPane.showMessageDialog(null, "Erro ao gravar dados no arquivo.: erro = " + erro);
    
}
    }
    
    public void carregarEmArquivo(){
       DefaultTableModel mdltable = (DefaultTableModel) tabelaTrab.getModel();
       mdltable.setNumRows(0);
       Trabalho t1;
    try{ 
    ObjectInputStream arquivoLer= new ObjectInputStream(new FileInputStream(new File("trabalho.txt")));
    arrayTrabalho= (ArrayList) arquivoLer.readObject();
        
    } 
    catch(FileNotFoundException erro){
        JOptionPane.showMessageDialog(null, "O arquivo não foi encontrado.: erro="+erro);
    }
    catch(ClassNotFoundException erro){
        JOptionPane.showMessageDialog(null, "Classe erro.: erro="+erro);
    }
catch(IOException erro){
    JOptionPane.showMessageDialog(null, "Erro ao ler dados no arquivo.: erro="+erro);
}
 
            Object campos[] = new Object[27]; //27 = tamanho 
            Iterator iterator = arrayTrabalho.iterator();
            while(iterator.hasNext()){
                t1 = (Trabalho) iterator.next();
                campos[0] = t1.getNome();
                campos[1] = t1.getDdd1();
                campos[2] = t1.getTelefone1();
                campos[3] = t1.getOperadora1();
                campos[4] = t1.getDdd2();
                campos[5] = t1.getTelefone2();
                campos[6] = t1.getOperadora2();
                campos[7] = t1.getDdd3();
                campos[8] = t1.getTelefone3();
                campos[9] = t1.getOperadora3();
                campos[10] = t1.getEmail1();
                campos[11] = t1.getEmail2();
                campos[12] = t1.getEmail3();
                campos[13] = t1.getRua();
                campos[14] = t1.getComplemento();
                campos[15] = t1.getRuaN();
                campos[16] = t1.getPais();
                campos[17] = t1.getCidade();
                campos[18] = t1.getBairro();
                campos[19] = t1.getCep();
                campos[20] = t1.getEstado();
                campos[21] = t1.getAniversario();
                campos[22] = t1.getEmpresa();
                campos[23] = t1.getSetor();
                campos[24] = t1.getReuniao1();
                campos[25] = t1.getReuniao2();
                campos[26] = t1.getReuniao3();
                
                mdltable.addRow(campos);
            }
            if(tabelaTrab.getRowCount() > 0){
                tabelaTrab.setRowSelectionInterval(tabelaTrab.getRowCount()-1,tabelaTrab.getRowCount()-1);
                setarCampos();
            }
        
    }
    
     private void diasParaAniver() throws ParseException{
        DateFormat df = new SimpleDateFormat ("dd/MM/yyyy");
        int diferenca;
        df.setLenient(false);
        Date d1 = df.parse (arrayTrabalho.get(tabelaTrab.getSelectedRow()).getAniversario());
        System.out.println (d1);
        Date d2 = new Date(System.currentTimeMillis());  
        System.out.println (d2);
        long dt = (d2.getTime() - d1.getTime()) + 3600000; // 1 hora para compensar horário de verão
        //System.out.println (dt / 86400000L); // passaram-se 67111 dias
        diferenca = (int) (dt / 86400000L);
        dias.setText(Math.abs(diferenca) + "");
        //return diferenca;
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JFormattedTextField aniver;
    private javax.swing.JTextField bairro;
    private javax.swing.JFormattedTextField cep;
    private javax.swing.JTextField cidade;
    private javax.swing.JTextField complemento;
    private javax.swing.JTextField ddd1;
    private javax.swing.JTextField ddd2;
    private javax.swing.JTextField ddd3;
    private javax.swing.JLabel dias;
    private javax.swing.JTextField email1;
    private javax.swing.JTextField email2;
    private javax.swing.JTextField email3;
    private javax.swing.JTextField empresa;
    private javax.swing.JTextField estado;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton9;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel217;
    private javax.swing.JLabel jLabel218;
    private javax.swing.JLabel jLabel219;
    private javax.swing.JLabel jLabel220;
    private javax.swing.JLabel jLabel221;
    private javax.swing.JLabel jLabel222;
    private javax.swing.JLabel jLabel223;
    private javax.swing.JLabel jLabel224;
    private javax.swing.JLabel jLabel225;
    private javax.swing.JLabel jLabel226;
    private javax.swing.JLabel jLabel227;
    private javax.swing.JLabel jLabel228;
    private javax.swing.JLabel jLabel229;
    private javax.swing.JLabel jLabel230;
    private javax.swing.JLabel jLabel231;
    private javax.swing.JLabel jLabel232;
    private javax.swing.JLabel jLabel233;
    private javax.swing.JLabel jLabel234;
    private javax.swing.JLabel jLabel235;
    private javax.swing.JLabel jLabel236;
    private javax.swing.JLabel jLabel237;
    private javax.swing.JLabel jLabel238;
    private javax.swing.JLabel jLabel239;
    private javax.swing.JLabel jLabel240;
    private javax.swing.JLabel jLabel241;
    private javax.swing.JLabel jLabel242;
    private javax.swing.JLabel jLabel243;
    private javax.swing.JLabel jLabel244;
    private javax.swing.JLabel jLabel245;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JTextField n;
    private javax.swing.JTextField nome;
    private javax.swing.JTextField operadora1;
    private javax.swing.JTextField operadora2;
    private javax.swing.JTextField operadora3;
    private javax.swing.JTextField pais;
    private javax.swing.JFormattedTextField reu1;
    private javax.swing.JFormattedTextField reu2;
    private javax.swing.JFormattedTextField reu3;
    private javax.swing.JTextField rua;
    private javax.swing.JTextField setor;
    private javax.swing.JTable tabelaTrab;
    private javax.swing.JTextField telefone1;
    private javax.swing.JTextField telefone2;
    private javax.swing.JTextField telefone3;
    // End of variables declaration//GEN-END:variables
}
