
package Classes;

import java.io.Serializable;


public class Amigo extends Contato implements Serializable{
    private String reuniao1;
    private String reuniao2;
    private String reuniao3;
    
    public  Amigo() {
    //    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public Amigo(String nome,String ddd1, String telefone1,String operadora1,String ddd2, String telefone2,String operadora2,String ddd3, String telefone3,String operadora3, String email1, String email2, String email3, String rua, String complemento, String ruaN, String pais, String cidade, String bairro, String cep, String estado, String aniversario, String r1,String r2, String r3){
        super(nome,ddd1,telefone1,operadora1,ddd2,telefone2,operadora2,ddd3,telefone3,operadora3,email1,email2,email3,rua,complemento,ruaN,pais,cidade,bairro,cep,estado,aniversario,"Amigo");
        this.reuniao1 = r1;
        this.reuniao2 = r2;
        this.reuniao3 = r3;
    }
    public void setAmigo(String nome,String ddd1, String telefone1,String operadora1,String ddd2, String telefone2,String operadora2,String ddd3, String telefone3,String operadora3, String email1, String email2, String email3, String rua, String complemento, String ruaN, String pais, String cidade, String bairro, String cep, String estado, String aniversario, String r1,String r2, String r3){
        this.setNome(nome);
        this.setDdd1(ddd1);
        this.setDdd2(ddd2);
        this.setDdd3(ddd3);
        this.setEmail1(email1);
        this.setEmail2(email2);
        this.setEmail3(email3);
        this.setOperadora1(operadora1);
        this.setOperadora2(operadora2);
        this.setOperadora3(operadora3);
        this.setAniversario(aniversario);
        this.setBairro(bairro);
        this.setCep(cep);
        this.setCidade(cidade);
        this.setComplemento(complemento);
        this.setRua(rua);
        this.setTelefone1(telefone1);
        this.setTelefone2(telefone2);
        this.setTelefone3(telefone3);
        this.setEstado(estado);
        this.setPais(pais);
        this.setRuaN(ruaN);
        this.reuniao1 = r1;
        this.reuniao2 = r2;
        this.reuniao3 = r3;
    }
   

    public String getReuniao1() {
        return reuniao1;
    }

    public String getReuniao2() {
        return reuniao2;
    }

    public String getReuniao3() {
        return reuniao3;
    }
}
