 
package Classes;

import java.io.Serializable;

public class Familia extends Contato{
    private String parentesco;

    public Familia() {
    }

    public Familia(String nome, String ddd1, String telefone1, String operadora1, String ddd2, String telefone2, String operadora2, String ddd3, String telefone3, String operadora3, String email1, String email2, String email3, String rua, String complemento, String ruaN, String pais, String cidade, String bairro, String cep, String estado, String aniversario,String parentesco) {
        super(nome, ddd1, telefone1, operadora1, ddd2, telefone2, operadora2, ddd3, telefone3, operadora3, email1, email2, email3, rua, complemento, ruaN, pais, cidade, bairro, cep, estado, aniversario, "Familia");
        this.parentesco = parentesco;
    }

    public void setFamilia(String nome,String ddd1, String telefone1,String operadora1,String ddd2, String telefone2,String operadora2,String ddd3, String telefone3,String operadora3, String email1, String email2, String email3, String rua, String complemento, String ruaN, String pais, String cidade, String bairro, String cep, String estado, String aniversario, String empresa, String parentesco){
        this.setNome(nome);
        this.setDdd1(ddd1);
        this.setDdd2(ddd2);
        this.setDdd3(ddd3);
        this.setEmail1(email1);
        this.setEmail2(email2);
        this.setEmail3(email3);
        this.setOperadora1(operadora1);
        this.setOperadora2(operadora2);
        this.setOperadora3(operadora3);
        this.setAniversario(aniversario);
        this.setBairro(bairro);
        this.setCep(cep);
        this.setCidade(cidade);
        this.setComplemento(complemento);
        this.setRua(rua);
        this.setTelefone1(telefone1);
        this.setTelefone2(telefone2);
        this.setTelefone3(telefone3);
        this.setEstado(estado);
        this.setPais(pais);
        this.setRuaN(ruaN);
        this.parentesco = parentesco;
        
    }
    
    public String getParentesco() {
        return parentesco;
    }

    void setFamilia(String text, String text0, String text1, String text2, String text3, String text4, String text5, String text6, String text7, String text8, String text9, String text10, String text11, String text12, String text13, String text14, String text15, String text16, String text17, String text18, String text19, String text20, String text21) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}

